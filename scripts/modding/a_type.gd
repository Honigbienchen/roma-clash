var _sub_type: String = "" setget ,sub_type
var _type: String = "" setget ,type

func _init(type: String, sub_type: String):
    self._type = type
    self._sub_type = sub_type

func sub_type() -> String:
    return _sub_type

func type() -> String:
    return _type

# return success
func from_json(data: Dictionary) -> bool:
    return false

func to_json() -> Dictionary:
    return {}

extends Node

func _ready():
    if GSettings.auto_collecting == false:
        $UserInformation/VBoxContainer/UserOutput.set_text(tr("Key_AutoRessourceCollectorText"))
        $UserInformation/VBoxContainer/UserOutputTimer.start()

func _on_MenuButton_pressed():
    get_tree().change_scene("res://scene/ui_menu.tscn")

func _on_UserOutputTimer_timeout():
        $UserInformation/VBoxContainer/UserOutput.set_text(tr(""))

#check:is it still needed?
func UserInfo():
    $UserInformation/VBoxContainer/UserOutput.set_text(tr("Key_UserInformationRessourcen"))
    $UserInformation/VBoxContainer/UserOutputTimer.start()

extends Node

func _ready():
    var file = File.new()
    file.open("res://data/language/changelog_" + GSettings.language() + ".txt", File.READ)
    var text = file.get_as_text()
    file.close()
    $ChangelogPanel/ScrollContainer/VBoxContainer/ChangelogText.set_text(text)

func _on_BackButton_pressed():
    get_tree().change_scene("res://scene/ui_menu.tscn")

var _data = {}

signal _data_changed(type, sub_type, obj)

func connect_on_change(obj, function):
    return self.connect("_data_changed", obj, function)

func emit_change(type: String = "", sub_type: String = "", obj = null):
    emit_signal("_data_changed", type, sub_type, obj)

func from_json(data: Dictionary):
    # type_name: buildings, resources
    for type in data.keys():
        # value_name: wood, iron
        for sub_type in data[type].keys():
            # do not load none existing types
            if not GRegistry.has_sub_type(type, sub_type):
                print("Warning: " + type + " > " + sub_type + " does not exist.")
                continue
            self.add_sub_type(type, sub_type)
            # array of saved data (dict)
            for sav in data[type][sub_type]:
                var obj = GRegistry.get_sub_type(type, sub_type).new()
                if obj.from_json(sav):
                    self._data[type][sub_type].append(obj)
                    self.emit_change(type, sub_type, obj)
                else:
                    print("Warning: failed to load (" + type + " > " + sub_type + "): " + str(sav))

func to_json() -> Dictionary:
    var res = {}
    # type: buildings, resources
    for type in self._data.keys():
        # sub_type: wood, iron
        res[type] = {}
        for sub_type in self._data[type].keys():
            res[type][sub_type] = []
            # instances of value_name
            for obj in self._data[type][sub_type]:
                res[type][sub_type].append(obj.to_json())

    return res

func add_object(obj):
    self.add_sub_type(obj.type(), obj.sub_type())
    self._data[obj.type()][obj.sub_type()].append(obj)
    self.emit_change(obj.type(), obj.sub_type(), obj)

func add_sub_type(type: String, sub_type: String):
    if not self.has_type(type):
        self._data[type] = {}
    if not self.has_sub_type(type, sub_type):
        self._data[type][sub_type] = []

func has_type(type: String) -> bool:
    return self._data.has(type)

func get_type(type: String, default = null) -> Dictionary:
    return self._data.get(type, default)

func has_sub_type(type: String, sub_type: String) -> bool:
    return self._data.has(type) and self._data[type].has(sub_type)

# return all instances of sub_type
func get_sub_type(type: String, sub_type: String, default = null):
    if not self.has_type(type):
        return default
    return self._data[type].get(sub_type, default)

func get_resource(resource: String):
    var res = GUser.poss.get_sub_type("resource", resource, [])
    if res.size() == 0:
        return null
    return res[0]

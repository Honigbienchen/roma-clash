extends Node

const BuildingTemplate = preload("res://scene/building_template.tscn")

func _ready():
    $HBoxContainerUI/ManagementMenuButton.get_popup().connect("id_pressed", self, "_Management_drop_down_pressed")
    $HBoxContainerUI/ConstructionMenuButton.get_popup().connect("id_pressed", self, "_Construction_drop_down_pressed")
    $PopupMenu.connect("id_pressed", self, "_PopupMenu")
    self._reload_option_menus()

func _reload_option_menus():
    # init administration drop down menu
    var popup = $HBoxContainerUI/ManagementMenuButton.get_popup()
    popup.clear()
    for name in GUser.poss.get_type("building", {}).keys():
        popup.add_item(tr(name))
    # init building
    popup = $HBoxContainerUI/ConstructionMenuButton.get_popup()
    popup.clear()
    var buildings = GRegistry.get_type("building")
    if buildings != null:
        for name in buildings.keys():
            var obj = buildings[name].new()
            var owned = GUser.poss.get_sub_type("building", name, []).size()
            popup.add_item(tr(name) + " " + str(owned) + "/" + str(obj.max_building()))
            popup.set_item_disabled(popup.get_item_count() - 1, owned == obj.max_building())
            #popup.set_item_disabled(popup.get_item_count() - 1, owned == obj.max_building() or owned >= obj.max_building())

func show_building_info(building, ident):
    if building == null:
        return # TODO: error handling
    # remove old building info
    var my_node = $HBoxContainer
    for child in my_node.get_children():
        my_node.remove_child(child)
    # show new
    var scene = BuildingTemplate.instance()
    if ident == true:
        scene.building = building
    else:
        scene.building = building.new()
    scene.fn_reload_main_ui = funcref(self, "_reload_option_menus")
    $HBoxContainer.add_child(scene)

func _Management_drop_down_pressed(id):
    var building = GUser.poss.get_type("building").keys()
    building = building[id]
    $PopupMenu.show()
    $PopupMenu.clear()
    $PopupMenu.add_item(building)
    $PopupMenu.set_item_disabled(0 ,true)
    for name in GUser.poss.get_sub_type("building", building):
        $PopupMenu.add_item(tr(name.sub_type() + " Level:" + str(name.level())))

func _PopupMenu(id):
    var type = $PopupMenu.get_item_text(0)
    self.show_building_info(GUser.poss.get_sub_type("building", type)[id - 1], true)

func _Construction_drop_down_pressed(id):
    self.show_building_info(GRegistry.get_type("building").values()[id], false)

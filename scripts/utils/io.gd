const SAVEGAME_FILE = "user://savegame.json"
const SETTINGS_FILE = "user://settings.json"
const PKG_DIR = "user://mods/"
const MOD_DIR = "res://mods/"

static func dir_files(path: String, extension=""):
    var files = []
    var dir = Directory.new()
    if dir.open(path) == OK:
        dir.list_dir_begin()
        var file_name = dir.get_next()
        while (file_name != ""):
            if !dir.current_is_dir() and (extension == "" or extension == file_name.get_extension()):
                files.append(file_name)
            file_name = dir.get_next()
    else:
        print("An error occurred when trying to access the path. '"+path+"'")
    return files    

static func dir_subdirs(path: String):
    var dirs = []
    var dir = Directory.new()
    if dir.open(path) == OK:
        dir.list_dir_begin()
        var file_name = dir.get_next()
        while (file_name != ""):
            if dir.current_is_dir():
                dirs.append(file_name)
            file_name = dir.get_next()
    else:
        print("An error occurred when trying to access the path. '"+path+"'")
    return dirs    

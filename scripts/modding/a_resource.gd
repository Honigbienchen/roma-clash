extends "res://scripts/modding/a_type.gd"

var _iconpath = ""
var value = 0

func _init(sub_type: String, iconpath: String).("resource", sub_type):
    self._iconpath = iconpath

func from_json(data: Dictionary) -> bool:
    self.value = data["value"]
    return true

func to_json() -> Dictionary:
    return {
        "value": self.value,
    }

func icon() -> String:
    return self._iconpath

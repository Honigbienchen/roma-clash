extends "res://scripts/modding/a_type.gd"

var _max_building: int = 0
var _level: int = 0
# upgrade costs
var _upgrading: Array = []
# produces
var _production: Array = []

func _init(sub_type: String, specification: Dictionary).("building", sub_type):
    self._level = 0
    self._max_building = specification.get("max_building", 0)
    # upgrade costs
    self._upgrading = specification.get("upgrading", [])
    # produces
    self._production = specification.get("production", [])

func from_json(data: Dictionary) -> bool:
    self._level = data["level"]
    return true

func to_json() -> Dictionary:
    return {
        "level": self._level,
    }

func level() -> int:
    return self._level
    
func max_level() -> int:
    return len(self._upgrading) - 1

func max_building() -> int:
    return self._max_building

func produce(sec: float) -> Dictionary:
    if len(self._production) <= self._level:
        return {}
    var ret = {}
    for item in self._production[self._level].keys():
        ret[item] = self._production[self._level][item] * sec
    return ret

# upgrade building
# returns empty string if valid
func upgrade() -> bool:
    var cost = self.upgrade_cost(self._level + 1)
    var cost_keys = cost.keys()
    var idx = 0
    var success = true
    while idx < cost_keys.size():
        var resource = cost_keys[idx]
        var own = GUser.poss.get_resource(resource)
        if own == null:
            # idx - 1, because we do not want to add this resource back
            idx -= 1
            success = false
            break
        own.value -= cost[resource]
        if own.value < 0:
            success = false
            break
        idx += 1
    if success:
        self._level += 1
        return true
    # failed, add already removed resources back
    while idx > 0:
        var resource = cost_keys[idx]
        GUser.poss.get_resource(resource).value += cost[resource]
    return false

# returns empty string if upgradeable
func can_upgrade() -> String:
    if self._level >= self.max_level():
        return "Max Level reached."
    var cost = self.upgrade_cost(self._level + 1)
    for res in cost.keys():
        var own = GUser.poss.get_resource(res)
        if own == null or cost[res] > own.value:
            return str(cost[res]) + " " + res + " required."
    return ""

# returns empty dict if level was not found
func upgrade_cost(level: int) -> Dictionary:
    if level > self.max_level():
        return {}
    return self._upgrading[level]

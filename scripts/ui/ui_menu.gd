extends Node

const io = preload("res://scripts/utils/io.gd")
var temp

func _on_BackGameButton_pressed():
    get_tree().change_scene("res://scene/main.tscn")

func _on_SaveButton_pressed():
    temp = 0
    $UserOutput/WindowDialog.show()
    $UserOutput/WindowDialog.set_position(Vector2(470, 323))
    $UserOutput/WindowDialog/Text.set_text(tr("Key_SaveQuery"))

func _on_LoadButton_pressed():
    temp = 1
    $UserOutput/WindowDialog.show()
    $UserOutput/WindowDialog.set_position(Vector2(470, 323))
    $UserOutput/WindowDialog/Text.set_text(tr("Key_LoadQuery"))

func _on_SettingsButton_pressed():
    get_tree().change_scene("res://scene/settings.tscn")

func _on_QuitButton_pressed():
    temp = 2
    $UserOutput/WindowDialog.show()
    $UserOutput/WindowDialog.set_position(Vector2(470, 323))
    $UserOutput/WindowDialog/Text.set_text(tr("Key_QuitQuery"))

func _on_YesButton_pressed():
    if temp == 1:
        GUser.load()
        $UserOutput/VBoxContainer/UserOutput.set_text(tr("Key_Load"))
        $UserOutput/VBoxContainer.show()
    else:
        GUser.save()
        $UserOutput/VBoxContainer/UserOutput.set_text(tr("Key_Save"))
        $UserOutput/VBoxContainer.show()
    
    if temp == 2:
        GUser.save()
        GSettings.save()
        get_tree().quit()
    
    $UserOutput/VBoxContainer/UserOutputTimer.start()
    $UserOutput/WindowDialog.hide()

func _on_NoButton_pressed():
    $UserOutput/WindowDialog.hide()

func _on_UserOutputTimer_timeout():
    $UserOutput/VBoxContainer.hide()

func _on_CreditsButton_pressed():
    get_tree().change_scene("res://scene/credits.tscn")

func _on_ChangelogButton_pressed():
    get_tree().change_scene("res://scene/changelog.tscn")

extends Node

var _wood_counter
var _iron_counter
var _stone_counter

var _dialog_wood_counter
var _dialog_iron_counter
var _dialog_stone_counter

func _ready():
    self._wood_counter = $UiHBoxContainer/Woodcounter
    self._iron_counter = $UiHBoxContainer/Ironcounter
    self._stone_counter = $UiHBoxContainer/Stonecounter
    self._dialog_wood_counter = $ResourceWindowDialog/VBoxContainer/WoodHBoxContainer/Woodcounter
    self._dialog_iron_counter = $ResourceWindowDialog/VBoxContainer/IronHBoxContainer/Ironcounter
    self._dialog_stone_counter = $ResourceWindowDialog/VBoxContainer/StoneHBoxContainer/Stonecounter
    
    GUser.poss.connect_on_change(self, "reload_popup_resource_ui")
    GUser.poss.connect_on_change(self, "reload_ui")
    GUser.update_resource()
    if GSettings.auto_collecting:
        # Start Rescources Timer
        $UiHBoxContainer/CollectResourcesTimer.start()
    self.reload_ui()

func _get_ressource_text(resource: String) -> String:
    var text = "0"
    var res = GUser.poss.get_resource(resource)
    if res != null:
        text = str(int(res.value))
    return text

func reload_ui(type = "", sub_type = "", obj = null):
    if type == "resource" and obj != null:
        if sub_type == "wood":
            self._wood_counter.set_text(str(obj.value))
        if sub_type == "stone":
            self._stone_counter.set_text(str(obj.value))
        if sub_type == "iron":
            self._iron_counter.set_text(str(obj.value))
    else:
        self._wood_counter.set_text(self._get_ressource_text("wood"))
        self._stone_counter.set_text(self._get_ressource_text("stone"))
        self._iron_counter.set_text(self._get_ressource_text("iron"))

func _on_CollectResourcesTimer_timeout():
    GUser.update_resource()
    $UiHBoxContainer/CollectResourcesTimer.start()

func _on_InfoButton_pressed():
    $ResourceWindowDialog.show()
    self.reload_popup_resource_ui()

func reload_popup_resource_ui(type = "", sub_type = "", obj = null):
    if type == "resource" and obj != null:
        if sub_type == "wood":
            self._dialog_wood_counter.set_text(str(obj.value))
        if sub_type == "stone":
            self._dialog_stone_counter.set_text(str(obj.value))
        if sub_type == "iron":
            self._dialog_iron_counter.set_text(str(obj.value))
    else:
        self._dialog_wood_counter.set_text(self._get_ressource_text("wood"))
        self._dialog_stone_counter.set_text(self._get_ressource_text("stone"))
        self._dialog_iron_counter.set_text(self._get_ressource_text("iron"))

func _on_CloseButton_pressed():
    $ResourceWindowDialog.hide()

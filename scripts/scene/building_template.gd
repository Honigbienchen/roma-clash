extends GridContainer

var building = null
var fn_reload_main_ui = null

func _ready():
    reload_ui()

func reload_ui():
    if self.building == null:
        return
    
    # name
    $Building_Name.set_text(tr(self.building.sub_type()))
    # level
    $Level.set_text(tr("Key_Level") + str(self.building.level()))

    # production
    var prod_str = ""
    var minute_prod = self.building.produce(60)
    for resource in minute_prod.keys():
        prod_str += tr(resource) + ": " + str(minute_prod[resource]) + " "
    
    $Production.set_text(tr("Production") + ": " + prod_str)

    # upgrade costs
    var next_level = self.building.level() + 1
    var upgrade_cost_str = ""
    if next_level <= self.building.max_level():
        upgrade_cost_str = tr("Key_BuildCost")
        var next_cost = self.building.upgrade_cost(next_level)
        for resource in next_cost.keys():
            upgrade_cost_str += tr(str(resource)) + ": " + str(next_cost[resource]) + " "
    $UpgradeCost.set_text(upgrade_cost_str)

    # rename upgrade button if level is 0
    if self.building.level() == 0:
        $UpgradeButton.set_text(tr("Key_BuildButton"))
    else:
        $UpgradeButton.set_text(tr("Key_UpgradeButton"))

    # disable upgrade button of upgrading is not possible
    var can_upgrade_msg = self.building.can_upgrade()
    if can_upgrade_msg != "":
        $UpgradeButton.set_disabled(true)
        $UpgradeButton.set_text($UpgradeButton.text + " " + can_upgrade_msg)

func _on_UpgradeButton_pressed():
    GUser.update_resource()
    if self.building.upgrade():
        # add new building to gdata
        if self.building.level() == 1:
            GUser.poss.add_object(self.building)
        reload_ui()
        fn_reload_main_ui.call_func()
        GUser.poss.emit_change()
    else:
        pass
        # TODO: show error message

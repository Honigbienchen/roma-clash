extends Node

const Io = preload("res://scripts/utils/io.gd")
const Possession = preload("res://scripts/struct/possession.gd")

var last_time = OS.get_unix_time()
var poss = Possession.new()

func _init():
    self.last_time = OS.get_unix_time()

func _ready():
    self.load()

func save():
    var file = File.new()
    file.open(Io.SAVEGAME_FILE, File.WRITE)
    file.store_line(JSON.print(self._to_json()))
    file.close()

func load():
    var file = File.new()
    if not file.file_exists(Io.SAVEGAME_FILE):
        return
    
    file.open(Io.SAVEGAME_FILE, File.READ)
    var data = JSON.parse(file.get_as_text()).result
    file.close()
    
    self._from_json(data)
    for res in GRegistry.get_type("resource").values():
        var new_res = res.new()
        if not self.poss.has_sub_type("resource", new_res.sub_type()):
            self.poss.add_object(res.new())

# loads available value, if not present uses default
func _from_json(data: Dictionary):
    # Todo: check if entries are existing, like name
    self.last_time = data.get("last_time", self.last_time)
    self.poss.from_json(data["possession"])

func _to_json() -> Dictionary:
    return {
        "last_time" : self.last_time,
        "possession" : self.poss.to_json(),
    }

func update_resource():
    var cur_time = OS.get_unix_time()
    var delay = cur_time - self.last_time
    self.last_time = cur_time

    for sub_type in self.poss.get_type("building", {}).keys():
        for building in self.poss.get_sub_type("building", sub_type):
            var produced = building.produce(delay)
            for resource in produced.keys():
                self.poss.add_sub_type("resource", resource)
                self.poss.get_sub_type("resource", resource)[0].value += produced[resource]
    self.poss.emit_change()

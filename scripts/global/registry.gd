extends Node

const AMod = preload("res://scripts/modding/a_mod.gd")
const Io = preload("res://scripts/utils/io.gd")

var _mods = {}
# dict[<type name>] -> dict[<name>] -> class
# {"buildings": {"house": house, "skyscraper": skyscraper},
#  "resource": {"money": money, "wood": wood}}
var types = {}

func _ready():
    self._load_all_pkgs()

func _load_all_pkgs():
    for pkg in self.get_available_pkgs():
        self.load_pkg(pkg)
    self._register_all_mods()

func get_available_pkgs():
    var pkgs = []
    for pkg in Io.dir_files(Io.PKG_DIR, "pck"):
        pkgs.append(pkg.trim_suffix(".pck"))
    return pkgs

func load_pkg(name: String):
    print("Package loading: " + name)
    if ProjectSettings.load_resource_pack(Io.PKG_DIR + name + ".pck"):
        print("Packaged loaded: " + name)
    else:
        print("Packaged loading failed: " + name)

func _register_all_mods():
    self._mods.clear()
    for mod_dir in Io.dir_subdirs(Io.MOD_DIR):
        self.register_mod(mod_dir)

func register_mod(mod_dir: String) -> bool:
    # Todo: check if mod dir exists
    print("Mod registering: " + mod_dir)
    var mod_cls = load(Io.MOD_DIR + mod_dir + "/mod.gd")
    if mod_cls == null:
        print("Mod registering failed: " + mod_dir)
        return false
    var mod = mod_cls.new()
    mod._name = mod_dir

    mod.on_register()
    self.print_mod_content(mod)
    self._add_types(mod)
    
    self._mods[mod.name()] = mod
    print("Mod registered: " + mod.name() + " - " + mod.version())
    mod._on_game_start()
    return true

func _add_types(mod: AMod):
    if mod.types.size() <= 0:
        print("Mod " + mod.name() + " has no types.")
        return
    for type_cls in mod.types:
        var obj = type_cls.new()
        if obj.type() == "" or obj.sub_type() == "":
            print("Warning: type or sub_type cannot be empty, skipping.")
            continue
        if self.has_sub_type(obj.type(), obj.sub_type()):
            print("Warning: Mod " + mod.name() + " overrides " + obj.sub_type())
        self.add_sub_type(obj.type(), obj.sub_type())
        self.types[obj.type()][obj.sub_type()] = type_cls

func get_mod(name: String) -> AMod:
    return self._mods.get(name, null)

func add_sub_type(type: String, sub_type: String):
    if not self.has_type(type):
        self.types[type] = {}
    if not self.has_sub_type(type, sub_type):
        self.types[type][sub_type] = []

func has_type(type: String) -> bool:
    return self.types.has(type)

func get_type(type: String, default = null) -> Dictionary:
    return self.types.get(type, default)

func has_sub_type(type: String, sub_type: String) -> bool:
    return self.types.has(type) and self.types[type].has(sub_type)

# return all instances of sub_type
func get_sub_type(type: String, sub_type: String, default = null):
    if not self.has_type(type):
        return default
    return self.types[type].get(sub_type, default)

static func print_mod_content(mod: AMod):
    print(mod.name() + " - " + mod.version() + ":")
    for type_cls in mod.types:
        var type = type_cls.new()
        print("  - " + type.type() + " | " + type.sub_type())

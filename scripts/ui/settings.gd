extends Control

var auto_collecting_new
var language_new

func _ready():
    self.auto_collecting_new = GSettings.auto_collecting
    self.language_new = GSettings.language()

    $AutomaticCollectorCheckButton.set_pressed(GSettings.auto_collecting)
    var popup = $LanguageHBox/LanguageOptionButton.get_popup()
    popup.clear()
    for lang in GSettings.AVAILABLE_LANG:
        popup.add_item(tr(lang))
    popup.connect("id_pressed", self, "_menuID")
    $LanguageHBox/LanguageOptionButton.set_text(tr(GSettings.language()))

func _on_BackButton_pressed():
    get_tree().change_scene("res://scene/ui_menu.tscn")

func _on_SaveButton_pressed():
    GSettings.auto_collecting = self.auto_collecting_new
    GSettings.update_lang(self.language_new)

    $UserInformation/VBoxContainer/UserOutput.set_text(tr("Key_Save"))
    $UserInformation/UserOutputTimer.start()
    self._ready()

func _on_CheckButton_pressed():
    self.auto_collecting_new = $AutomaticCollectorCheckButton.is_pressed()

func _on_LanguageOptionButton_item_selected(idx):
    self.language_new = GSettings.AVAILABLE_LANG[idx]

func _on_UserOutputTimer_timeout():
    $UserInformation/VBoxContainer/UserOutput.set_text("")

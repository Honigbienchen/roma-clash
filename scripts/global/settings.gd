extends Node

const Io = preload("res://scripts/utils/io.gd")

var _language: String = "lang_en"
var auto_collecting: bool = true

const AVAILABLE_LANG = ["lang_en", "lang_de"]

func _ready():
    self.load()

func save():
    var file = File.new()
    file.open(Io.SETTINGS_FILE, File.WRITE)
    file.store_line(JSON.print(self._to_json()))
    file.close()

func load():
    var file = File.new()
    if not file.file_exists(Io.SETTINGS_FILE):
        return

    file.open(Io.SETTINGS_FILE, File.READ)
    var data = JSON.parse(file.get_as_text()).result
    file.close()
    self._from_json(data)

# loads available value, if not present uses default
func _from_json(data: Dictionary):
    self.update_lang(data.get("language", self._language))
    self.auto_collecting = data.get("auto_collecting", self.auto_collecting)

func _to_json() -> Dictionary:
    return {
        "language": self._language,
        "auto_collecting": self.auto_collecting,
    }

func language() -> String:
    return self._language

# Warning: updates the translatIon server
# new_lang: in form of "lang_<language code>"
func update_lang(new_lang: String):
    if self.AVAILABLE_LANG.has(new_lang):
        self._language = new_lang
        TranslationServer.set_locale(self._language.right(5))

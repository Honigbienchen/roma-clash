var _name: String = ""
var _description: String = ""
var _author: String = ""
var _version: String = ""

const NAME = ""

var types = {}

func _init(description: String, author: String, version: String):
    self._description = description
    self._author = author
    self._version = version

func name() -> String:
    return self._name

func description() -> String:
    return self._description

func author() -> String:
    return self._author

func version() -> String:
    return self._version

func on_register():
    pass

# signal callbacks

func _on_game_start():
    pass
